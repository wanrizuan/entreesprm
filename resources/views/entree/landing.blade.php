<html>
<head>
  @title()
  <link rel="icon" type="image/png" href="{{ asset('packages/threef/entree/img/favicon.png') }}">
  <link rel="stylesheet" type="text/css" href="{{asset('/packages/threef/entree/logincss/bootstrap-theme.css')}}">
  <!-- <link rel="stylesheet" type="text/css" href="logincss/bootstrap-theme.min.css">
  <link rel="stylesheet" type="text/css" href="logincss/bootstrap.css">
  <link rel="stylesheet" type="text/css" href="logincss/bootstrap.min.css"> -->
  <link rel="stylesheet" type="text/css" href="{{asset('/packages/threef/entree/logincss/mainlog.css')}}">
  <link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>
   <!-- <script src="loginjs/bootstrap.js"></script>
  // <script src="loginjs/bootstrap.min.js"></script>
  // <script src="loginjs/npm.js"></script>
  // <script src="loginjs/jquery-1.9.1.min.js"></script>
  // <script src="loginjs/jssor.slider.debug.js"></script>
  // <script src="loginjs/jssor.slider.min.js"></script>
  // <script src="loginjs/jssor.slider.mini.js"></script>
  // <script src="loginjs/loginmain.js"></script> -->

  <!-- <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css"> -->
  <link rel="stylesheet" href="{{ asset('/packages/threef/entree/logincss/bootstrap.min.css')}}">

  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

  <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
  <style type="text/css">
    .alert-error {
        color: #ff0000;
        background-color: #ffcccc;
        border-color: #800000;
    }
  </style>
</head>
<body id="mainbody" style="background-color:#848484;">
  <div id="toplogonav" class="">
    <img id="toplogo" src="{{ asset('packages/threef/entree/img/logosprm.png') }}">
    &nbsp
 </div>

<br>
<br>
<br>
<br>
<div id="sprmtitle" class=""> Suruhanjaya Pencegahan Rasuah Malaysia </div>
<center>
  <div id="container" class="">
    <div id="sistemtitle" class="">
      <!-- <h1 id="title"><b>SISTEM PENGURUSAN EKSIBIT</b></h1><br><br><br> -->
      <img id="ems" src="{{ asset('packages/threef/entree/img/ems.png') }}">
      <br>
    </div>

    <div id="login-pic" class="col-sm-6 carousel slide" data-ride="carousel">
      <ol class="carousel-indicators">
    <li data-target="#login-pic" data-slide-to="0" class="active"></li>
    <li data-target="#login-pic" data-slide-to="1"></li>
    <li data-target="#login-pic" data-slide-to="2"></li>

  </ol>

  <!-- Wrapper for slides -->
  <div class="carousel-inner" role="listbox">
    <div class="item active">
      <img id="leftpic" src="{{ asset('packages/threef/entree/img/leftpic.jpg') }}" alt="Chania">
    </div>

    <div class="item">
      <img id="leftpic" src="{{ asset('packages/threef/entree/img/rfid.jpg') }}" alt="Chania">
    </div>

    <div class="item">
      <img id="leftpic" src="{{ asset('packages/threef/entree/img/detail.jpg') }}" alt="Flower">
    </div>

  </div>

  <!-- Left and right controls -->
  <a class="left carousel-control" href="#login-pic" role="button" data-slide="prev">
    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
    <span class="sr-only">Previous</span>
  </a>
  <a class="right carousel-control" href="#login-pic" role="button" data-slide="next">
    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
    <span class="sr-only">Next</span>
  </a>
    </div>

    <div id="loginbox" class="col-sm-6">
      <div id="loginbox-head" class="">
       <b>Log Masuk</b>
      </div>
      <div id="loginbox-body" class="">
        <center>
          @include('orchestra/foundation::components.messages')
          {!! Form::open(['url' => handles('threef::login'), 'action' => 'POST', 'class' => 'form-signin']) !!}


          <div class="form-group">
              {!! Form::input('text', 'username', old('username'), ['required' => true, 'class' => 'form-control', 'placeholder' => 'ID Nama']) !!}


          </div>

          <div class="form-group">
            {!! Form::input('password', 'password', '', ['required' => true, 'class' => 'form-control', 'placeholder' => trans('entree::entree.login.password')]) !!}

          </div>
          <div class="form-group">
              <button id="login-btn1" class="btn btn-primary">Log Masuk</button>
          {!! Form::close() !!}
              <div id="kskus-btn1" class="btn btn-primary">Permohonan KS KUS</div>
          </div>


        </center>
      </div>
      <br>

    </div>

  </div>
<br>
 <div id="kskus-div" class="">
    <!-- <div id="kskus-btn"><a href="handles('entree::borrowing') !!}" class="btn " style="color:black;"> <strong>Permohonan KS KUS</strong></a></div> -->
   <!-- <button id="kskus-btn" type="button" class="btn btn-primary">Permohonan KS KUS</button> -->
 </div>

</center>
<center>
  <div class="col-sm-12 text-center text-xs container-fluid" style="background-color:GAINSBORO;">
            <h6>PENAFIAN: Paparan terbaik menggunakan pelayar Mozilla Firefox versi 40.0 dan ke atas serta  pelayar Chrome versi 50.0 dan ke atas dengan resolusi skrin 1366x768. <br/>Hak Cipta Terpelihara © 2016 Suruhanjaya Pencegahan Rasuah Malaysia.</h6>
  </div>
</center>


<script type="text/javascript">


  $("#kskus-btn1").click(function(){

         window.location = "{!! handles('entree::borrowing') !!}";
  });


</script>
</body>
</html>
