
@extends('entree::layouts.main')
@push('content.style')

@stop
@section('content')
  <div class="row">
      <div class="col-lg-12">
          <h1 class="page-header">Dashboard</h1>
      </div>
      <!-- /.col-lg-12 -->
  </div>
  <div class="row">
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-primary">

              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-comments fa-5x"></i>
                      </div>

                      <div class="col-xs-9 text-right">
                          <div>View Details</div>
                      </div>

                  </div>
              </div>

          </div>
      </div>
    </a>
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-green">
              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-tasks fa-5x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div>View Details</div>
                      </div>
                  </div>
              </div>

          </div>
      </div>
    </a>
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-yellow">
              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-shopping-cart fa-5x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div>View Details</div>
                      </div>
                  </div>
              </div>

          </div>
      </div>
    </a>
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-red">
              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-support fa-5x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div>View Details</div>
                      </div>
                  </div>
              </div>

          </div>
      </div>
    </a>
  </div>
@endsection
@push('content.script')
<script type="text/javascript">

</script>
@stop