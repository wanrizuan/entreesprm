<!DOCTYPE html>
<html lang="en">
    <head>
        @include('entree::layouts._header')
    </head>
    <body>
        <div id="wrapper">

            @include('entree::layouts._side_menu')


            @yield('widget')

            <div id="page-wrapper">

                @include('entree::layouts.components.message')
            <!-- include('orchestra/foundation::components.messages') -->


            <!-- include('entree::layouts.components._statistic') -->

            <!-- include('entree::layouts.components._notification') -->
            @yield('content')
            </div>

            <div id="">
                @include('entree::layouts._footer')
            </div>


        </div>



    </body>
</html>
