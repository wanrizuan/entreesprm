<!-- Footer Start -->

<!-- Footer END -->

#{{ $asset = Asset::container('entree::footer') }}




<!-- asset->script('bootstrap1', asset ('packages/threef/entree/loginjs/bootstrap.js')) }} -->
#{{ $asset->script('jquery-min', asset ('packages/threef/entree/loginjs/jquery-1.11.3.min.js')) }}
<!-- asset->script('bootstrap-min', asset ('packages/threef/entree/loginjs/bootstrap.min.js')) }} -->
<!-- asset->script('npm', asset ('packages/threef/entree/loginjs/npm.js')) }} -->

<!-- asset->script('jquery-min', asset ('packages/threef/entree/js/jquery.min.js')) }} -->
#{{ $asset->script('jssor-slider-debug', asset ('packages/threef/entree/loginjs/jssor.slider.debug.js')) }}
#{{ $asset->script('jssor-slider-min', asset ('packages/threef/entree/loginjs/jssor.slider.min.js')) }}
#{{ $asset->script('jssor-slider-mini', asset ('packages/threef/entree/loginjs/jssor.slider.mini.js')) }}
#{{ $asset->script('loginmain', asset ('packages/threef/entree/loginjs/loginmain.js')) }}

#{{ $asset->script('bootstrap', asset('packages/orchestra/foundation/vendor/bootstrap/js/bootstrap.min.js')) }}
#{{ $asset->script('jquery-ui', asset('packages/orchestra/foundation/vendor/jquery.ui/jquery.ui.js')) }}
<!-- asset->script('orchestra', asset('packages/orchestra/foundation/js/orchestra.min.js'), array('bootstrap', 'jquery-ui')) }} -->
#{{ $asset->script('jui-toggleSwitch', asset('packages/orchestra/foundation/vendor/delta/js/jquery-ui.toggleSwitch.js'), array('jquery-ui')) }}
#{{ $asset->script('select2', asset('packages/orchestra/foundation/components/select2/select2.min.js')) }}

#{{ $asset->script('timepicker', asset ('packages/threef/entree/loginjs/bootstrap-timepicker.js')) }}   <!-- timepicker-->

#{{ $asset->script('calendar', asset('/packages/threef/entree/dist/js/responsive-calendar.js')) }}



<!-- asdasdas -->

{!! $asset->styles() !!}
{!! $asset->scripts() !!}


<?php
 $cuti = DB::select(DB::raw("SELECT id,event as event , DATE_FORMAT(date_from,'%Y-%m-%d') AS 'start',DATE_FORMAT(date_to,'%Y-%m-%d') AS 'end' FROM lkp_calendar WHERE status = 1;"));
// dd($cuti);exit;
 $acara = array();
 foreach ($cuti as $key => $value) {
 	$cth = getDatesFromRange($value->start, $value->end);
 	// dd($cth);

 	foreach ($cth as $a ) {

 		// $acara['event'] = $value->event;
 		// $acara['date'] = $a;
 		$acara[$a]['event'] =  $value->event;
        $acara[$a]['start'] =  $a;
        $acara[$a]['end'] =  $value->end;
 		
 	}

 	
 }
 // dd($acara);exit;

function getDatesFromRange($start, $end){
    $dates = array($start);
    while(end($dates) < $end){
        $dates[] = date('Y-m-d', strtotime(end($dates).' +1 day'));
    }
    return $dates;
}


?>
@placeholder('orchestra.layout: footer')

<script>
// jQuery(function on_page_ready($) { 'use strict';
//   Javie.trigger("orchestra.ready: {!! Request::path() !!}");
// });
</script>
<script type="text/javascript">
    $(window).load(function(){
        $('#myMessage').modal('show');
    });

    setTimeout(function(){
      $('#myMessage').modal('hide')
    }, 3000);
</script>

<script type="text/javascript">
 $(document).ready(function () {
    function addLeadingZero(num) {
    if (num < 10) {
      return "0" + num;
    } else {
      return "" + num;
    }
  }
         $(".responsive-calendar").responsiveCalendar({
          time: '{{$ldate = date('Y-m')}}',
          events: {
          	
          <?php //$d = json_encode($acara); //dd($d);?>

          @foreach($acara as $key => $evnt)

          	"{{$evnt['start']}}" : {"dayEvents" : "Acara : {{$evnt['event']}}"},
          
          @endforeach
           

              "2100-08-12": {}
           },
             onActiveDayHover:
          function(events) {
            var thisDayEvent, key;

      key = $(this).data('year')+'-'+addLeadingZero( $(this).data('month') )+'-'+addLeadingZero( $(this).data('day') );

        thisDayEvent = events[key];


        document.getElementById("headv").innerHTML=thisDayEvent.dayEvents;

          },
           onDayHover:
          function(events) {

          document.getElementById("headv").innerHTML='';

          }
        });
      });

</script>

<script type="text/javascript">
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
</script>



@stack('entree.footer')
