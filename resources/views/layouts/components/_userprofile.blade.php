<?php
$user = app('auth')->user();

use Orchestra\Contracts\Auth\Guard;

$roles = Auth::roles();
// dd($roles);
?>
@inject('user', 'Illuminate\Contracts\Auth\Authenticatable')
<ul class="nav1 navbar-top-links navbar-right">
    @unless(is_null($user))
    <li class="dropdown">
      @foreach ($roles as $key => $role)
      <!-- role }} -->
      @endforeach
    </li>

    <li class="dropdown">
        <a class="dropdown-toggle" data-toggle="dropdown" href="#">
          <i class="fa fa-user fa-fw"></i>
          <span class="label label-default">{{ $user->username }}
          </span>  <i class="fa fa-caret-down"></i>
        </a>

        <ul class="dropdown-menu dropdown-user">
            <li><a href="{!! handles('entree::manage/showprofile') !!}"><i class="fa fa-user fa-fw"></i> Profil</a>
            </li>
            <li><a href="{!! handles('entree::manage/password') !!}"><i class="fa fa-gear fa-fw"></i> Tukar Katalaluan</a>
            </li>
            <li><a href="{!! handles('entree::manage/manual') !!}"><i class="fa fa-download fa-fw"></i> Manual Pengguna </a>
            </li>
            <li class="divider"></li>
            <li><a href="{!! handles('entree::logout') !!}"><i class="fa fa-sign-out fa-fw"></i> Log Keluar</a>
            </li>
        </ul>
    </li>

    @endunless
  </ul>