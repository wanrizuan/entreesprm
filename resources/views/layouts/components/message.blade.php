<?php


$format = <<<MESSAGE
<div class="modal fade " tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" id="myMessage">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="alert alert-:key" >
        <strong> :message </strong>
      </div>
    </div>
  </div>
</div>

MESSAGE;



$message = app('orchestra.messages')->retrieve();

if ($message instanceof Orchestra\Messages\MessageBag) :
	$message->setFormat($format);

	foreach (['error', 'info', 'success','warning','danger'] as $key) :
		if ($message->has($key)) :
			echo implode('', $message->get($key));
		endif;
	endforeach;
endif;
