<?php

$keluar = DB::table('rfid_transaction')->where('fk_lkp_status',11)->count();
$masuk = DB::table('rfid_transaction')->where('fk_lkp_status',12)->count();
$mohon = DB::table('rfid_transaction')->where('fk_lkp_status',6)->count();
$rfidcount = DB::table('rfid_transaction')->count(); //  jumlah ks-kus yg direkodkan

?>

  <div class="row" id="dashboard-menu">
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-primary">

              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-arrow-circle-down fa-4x"></i>
                      </div>

                      <div class="col-xs-9 text-right">
                          <div>Jumlah Pergerakan KS Keluar</div>
                          <span class="badge">{{ $keluar }}</span>
                      </div>

                  </div>
              </div>

          </div>
      </div>
    </a>
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-green">
              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-arrow-circle-up fa-4x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div>Jumlah Pergerakan KS Masuk</div>
                          <span class="badge">{{ $masuk }}</span>
                      </div>
                  </div>
              </div>

          </div>
      </div>
    </a>
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-yellow">
              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-suitcase fa-4x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div>Jumlah Permohonan Pinjaman KS KUS</div>
                          <span class="badge">{{ $mohon }}</span>
                      </div>
                  </div>
              </div>

          </div>
      </div>
    </a>
    <a href="#">
      <div class="col-lg-3 col-md-6">
          <div class="panel panel-red">
              <div class="panel-heading">
                  <div id="box1" class="row">
                      <div class="col-xs-3">
                          <i class="fa fa-list-ul fa-4x"></i>
                      </div>
                      <div class="col-xs-9 text-right">
                          <div>Jumlah KS KUS yang direkodkan (RFID)</div>
                          <span class="badge">{{ $rfidcount }}</span>
                      </div>
                  </div>
              </div>

          </div>
      </div>
    </a>
  </div>