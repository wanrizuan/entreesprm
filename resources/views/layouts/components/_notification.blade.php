<?php
    $settime = DB::table('lkp_set_time')->where('status',1)->first();
    $ksaktif_1 = DB::table('rfid')->where('fk_lkp_status',13)->count();
    $ksaktif_2 = DB::table('rfid')->where('fk_lkp_status',15)->count();

    $kskus_1 = DB::table('rfid')->where('fk_lkp_status',25)->count();

    $eksibit_1 = DB::table('rfid')->where('fk_lkp_status',18)->count();
    $eksibit_2 = DB::table('rfid')->where('fk_lkp_status',20)->count();


?>
<div class="row">
<section class="panel panel-default">
    <header class="panel-heading"><b>Notifikasi</b></header>
    <br/>
    <div class="panel-group">
        <div class="row" id="">

            <div class="col-xs-6 col-md-4">
                <!-- tiada pergerakan keluar ks astif -->
                <a href="#">
                      <div class="panel-notify panel-primary">

                          <div class="panel-heading-notify">
                              <div id="" class="row">
                                  <div class="col-xs-12">
                                      <h6>Tiada Pergerakan Keluar KS Aktif Selepas {{$settime->hour}} Jam <span class="badge pull-right">{{ $ksaktif_1 }}</span></h6>
                                  </div>

                              </div>

                          </div>

                      </div>

                </a>

                <!-- pergerakan aktif tak sah -->
                <a href="#">
                  <div class="">
                      <div class="panel-notify panel-primary">

                          <div class="panel-heading-notify">
                              <div id="" class="row">
                                  <div class="col-xs-12">
                                      <h6>Pergerakan KS Aktif Tidak Sah <span class="badge pull-right">{{ $ksaktif_2}}</span></h6>

                                  </div>

                              </div>
                          </div>

                      </div>
                  </div>
                </a>

            </div>

            <div class="col-xs-6 col-md-4">

                <!-- pergerakan ks kus tidak sah -->
                <a href="#">
                  <div class="">
                      <div class="panel-notify panel-primary">

                          <div class="panel-heading-notify">
                              <div id="" class="row">
                                  <div class="col-xs-12">
                                      <h6>Pergerakan KS Kus Tidak Sah <span class="badge pull-right">{{ $kskus_1 }}</span></h6>

                                  </div>

                              </div>
                          </div>

                      </div>
                  </div>
                </a>

            </div>

            <div class="col-xs-6 col-md-4">

                <!-- pergerakan keluar eksibit -->

                <a href="#">
                  <div class="">
                      <div class="panel-notify panel-primary">

                          <div class="panel-heading-notify">
                              <div id="" class="row">
                                  <div class="col-xs-12">
                                      <h6>Tiada Pergerakan Keluar Eksibit Selepas {{$settime->hour}} Jam <span class="badge pull-right">{{ $eksibit_1}}</span></h6>

                                  </div>

                              </div>
                          </div>

                      </div>
                  </div>
                </a>

                <!-- pergerakan eksibit tidak sah -->

                <a href="#">
                  <div class="">
                      <div class="panel-notify panel-primary">

                          <div class="panel-heading-notify">
                              <div id="" class="row">
                                  <div class="col-xs-12">
                                      <h6>Pergerakan Eksibit Tidak Sah <span class="badge pull-right">{{ $eksibit_2}}</span></h6>

                                  </div>

                              </div>
                          </div>

                      </div>
                  </div>
                </a>

            </div>

        </div>

    </div>
</section>
</div>
