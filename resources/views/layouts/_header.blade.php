<meta charset="utf-8">
@title()
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="{{ memorize('site.description', 'Orchestra Platform') }}">
<meta name="author" content="{{ memorize('site.author', 'Orchestra Platform') }}">
<meta name="csrf-token" content="{{ csrf_token() }}">
<link rel="icon" type="image/png" href="{{ asset('packages/threef/entree/img/favicon.png') }}">

<!-- Le HTML5 shim, for IE6-8 support of HTML elements -->
<!--[if lt IE 9]>
  <script src="{!! asset('packages/orchestra/foundation/components/html5shiv/html5shiv.min.js') !!}"></script>
<![endif]-->
<!-- <link href="https://fonts.googleapis.com/css?family=Roboto:400,700,300,500" rel="stylesheet" type="text/css"> -->

#{{ $asset = Asset::container('entree::header') }}


<link href='https://fonts.googleapis.com/css?family=Oswald' rel='stylesheet' type='text/css'>

#{{ $asset->style('picker2', asset('/packages/threef/entree/logincss/bootstrap-timepicker.css')) }}
#{{ $asset->style('picker1', asset('/packages/threef/entree/logincss/bootstrap-timepicker.min.css')) }}

#{{ $asset->style('jquery-dataTable', asset('/packages/threef/entree/bower_components/datatables/media/css/jquery.dataTables.css')) }}
#{{ $asset->style('bootstrap-min', asset('/packages/threef/entree/logincss/bootstrap.min.css')) }}
#{{ $asset->style('mainlog', asset('/packages/threef/entree/logincss/mainlog.css')) }}
#{{ $asset->style('main', asset('/packages/threef/entree/logincss/main.css')) }}
#{{ $asset->style('sb-admin', asset('/packages/threef/entree/dist/css/sb-admin-2.css')) }}

#{{ $asset->style('bootstrap-theme', asset('/packages/threef/entree/logincss/bootstrap-theme.css')) }}
#{{ $asset->style('bootstrap-theme-min', asset('/packages/threef/entree/logincss/bootstrap-theme.min.css')) }}
#{{ $asset->style('bootstrap', asset('/packages/threef/entree/logincss/bootstrap.css')) }}


#{{ $asset->style('font', asset('https://fonts.googleapis.com/css?family=Oswald')) }}
#{{ $asset->style('font-awesome', asset('/packages/threef/entree/bower_components/font-awesome/css/font-awesome.css')) }}

#{{ $asset->style('jquery-ui2', asset('/packages/orchestra/foundation/vendor/delta/theme/jquery-ui.css')) }}
#{{ $asset->style('select2', asset('/packages/orchestra/foundation/components/select2/select2.css')) }}

#{{ $asset->style('calendar', asset('/packages/threef/entree/dist/css/responsive-calendar.css')) }}




{!! $asset->styles() !!}
{!! $asset->scripts() !!}

<script>
// Javie.detectEnvironment(function () {
//   return "{!! app('env') !!}";
// });
</script>
<?php
$user = app('auth')->user();

use Orchestra\Contracts\Auth\Guard;

$roles = Auth::roles();
// dd($roles);
?>
@inject('user', 'Illuminate\Contracts\Auth\Authenticatable')

<!-- <nav id="spe-nav" class="navbar navbar-default navbar-static-top img-responsive img-fluid" role="navigation" style="margin-bottom: 0"> -->
<nav id="spe-nav1" class="img-responsive img-fluid" role="navigation" style="">
  <div  class="navbar-header ">

    <img id="" src="{{ asset('packages/threef/entree/img/bannersprmnew.png') }}" class="img-responsive" width="100%" height="100%">
    <!-- <img id="navbanner" src="sset('packages/threef/entree/img/bannersprm5.png') }}" class="img-responsive" width="1400px" height="140px"> -->
      <!-- <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
      </button> -->

  </div>

  <br><br><br><br>
  </nav>
  @include('entree::layouts.components._userprofile')


@placeholder("orchestra.layout: header")
@stack('entree.header')
