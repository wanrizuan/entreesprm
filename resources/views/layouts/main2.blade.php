<!DOCTYPE html>
<html lang="en">
    <head>
        @include('entree::layouts._header')
    </head>
    <body>
        <div id="wrapper">

            @yield('widget')


            <div id="page-wrapper">
            <!-- include('orchestra/foundation::components.messages') -->
            @include('entree::layouts.components.message')
            @yield('content')
            </div>

            <div id="">
                @include('entree::layouts._footer')
            </div>


        </div>



    </body>
</html>

<!-- <html lang="en">
    <head>
        include('orchestra/foundation::layouts._header')
        stack('threef.style')
        <link rel="shortcut icon" type="image/x-icon" href="/fav.ico">
        <title>SPRM - EMS</title>
    </head>
    <body >
        yield('body')
        <div class="row">
            <div class="col-md-12 text-center" style="position: absolute;bottom: 5px;" >
                include('orchestra/foundation::layouts._footer')
            </div>
            stack('threef.footer')
        </div>
    </body>
</html> -->

