<?php
use Orchestra\Contracts\Auth\Guard;
//use Orchestra\Model\Role;
use Threef\Entree\Http\User\Role;
// use DB;

$roles = Auth::roles();
$user_id = Auth::user()->id;

$lkpdepartment = DB::table('user_profile')
->join('lkp_department','lkp_department.id','=','user_profile.fk_lkp_department')
->join('lkp_state','lkp_state.id','=','user_profile.fk_lkp_state')
->leftJoin('lkp_branch','lkp_branch.id','=','user_profile.fk_lkp_branch')
->where('user_profile.fk_user',$user_id)
->select('lkp_department.id','lkp_department.description','lkp_state.description as state','lkp_branch.description as branch')
->first();

?>

@if(Auth::isAny([
    Role::administrator()->name,
    Role::admin()->name,
    Role::pengarahBPRTM()->name,
    Role::timbPengBPRTM()->name,
    Role::ketuaCAROS()->name,
    Role::CarosOfficer()->name,
    Role::pprHQ()->name,
    Role::pprNegeri()->name,
    Role::ioHQ()->name,
    Role::ioNegeri()->name,
    Role::pengarahNegeri()->name,
    Role::ketuaSiasatanNegeri()->name,
    Role::timbPengOperasi()->name,
    Role::PegSprmHQ()->name,
    Role::PegawaiSPRMNegeri()->name,
    Role::pengBahagian()->name,
    Role::pprCawHQ()->name,
    Role::pprCawNeg()->name,
    Role::pprHQSiasatan()->name,
    Role::ketuaCawNeg()->name,
    Role::ketuaCaw()->name,
    Role::timbPeng()->name

    ]))


<div id="side-bar" class="navbar-default sidebar" role="navigation">

<!-- paparan user role  -->

    <div id="side-bar-mid" class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">
            <li>
                <a href="#profile1" data-toggle="collapse" ><i class="fa fa-user"></i> {{ Auth::user()->fullname}}</a>
                <ul id="profile1" class="nav nav-second-level panel-collapse collapse in" >
                    <div class="panel panel-primary">
                    <div class="box-body box-profile">
                      <p class="text-left" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;"><small>
                        @foreach ($roles as $key => $role)
                          {{ $role }}
                        @endforeach
                      </small></p>
                      <p class="text-left" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;"><small>{{ $lkpdepartment->description}}</small></p>
                      <p class="text-left" style="font-family:Helvetica Neue,Helvetica,Arial,sans-serif;"><small>
                        <?php
                            if($lkpdepartment->branch !=''){
                                echo "".$lkpdepartment->branch;
                            }else{

                            }
                        ?>
                        </small>
                      </p>
                    </div>
                    </div>

                </ul>
            </li>
        </ul>
    </div>

<!-- end paparan user role -->
<!-- start menu -->
    <div id="side-bar-mid" class="sidebar-nav navbar-collapse">
        <ul class="nav" id="side-menu">

            <li>
                <a href="{!! handles('entree::home') !!}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
            </li>

            <!-- PPR HQ Siasatan tak leh nampak -->
            @if(Auth::isNot([ Role::pprHQSiasatan()->name ]))
            <li>
                <a href="#barsearch" data-toggle="collapse" ><i class="fa fa-folder-o fa-fw"></i> MAKLUMAT KS<span class="fa arrow"></span></a>
                <ul id="barsearch" class="nav nav-second-level panel-collapse collapse" >
                    <li id="">
                        <a href="{!! handles('entree::rfid/ks') !!}">Carian</a>
                    </li>
                     <li id="">
                        <a href="{!! handles('entree::rfid/lostfound') !!}">Hilang & Jumpa</a>
                    </li>
                </ul>
            </li>
            @else
            <li>
                <a href="#barsearch" data-toggle="collapse" ><i class="fa fa-folder-o fa-fw"></i> MAKLUMAT KS<span class="fa arrow"></span></a>
                <ul id="barsearch" class="nav nav-second-level panel-collapse collapse" >
                    <li id="">
                        <a href="{!! handles('entree::rfid/ks') !!}">Carian</a>
                    </li>
                </ul>
            </li>
            @endif
            <!-- end PPR HQ Siasatan tak leh nampak-->
            <!-- start KS AKTIF -->
            @if(Auth::isAny([
                Role::administrator()->name,
                Role::admin()->name,
                Role::pengarahBPRTM()->name,
                Role::timbPengBPRTM()->name,
                Role::ketuaCAROS()->name,
                Role::CarosOfficer()->name,
                Role::pprNegeri()->name,
                Role::pprCawNeg()->name,
                Role::pprHQ()->name,
                Role::pprCawHQ()->name,
                Role::pprHQSiasatan()->name
            ]))
            <li>
                <a href="#barksaktif" data-toggle="collapse" ><i class="fa fa-bar-chart-o fa-fw"></i> KS AKTIF<span class="fa arrow"></span></a>
                <ul id="barksaktif" class="nav nav-second-level panel-collapse collapse" >
                    <li id="">
                        <a href="{!! handles('entree::rfid/register/1') !!}">Pendaftaran RFID</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/ks-item/1') !!}">Kemaskini</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::transaction/movementout/filtering/1') !!}">Pergerakan Keluar</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::transaction/movementin/filtering/1') !!}">Pergerakan Masuk</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/move-rfid-gantry/1') !!}">Log Pergerakan RFID / Gantry</a>
                    </li>
                </ul>
            </li>
            @elseif(Auth::isAny([

            ]))

            <li>
                <a href="#barksaktif" data-toggle="collapse" ><i class="fa fa-bar-chart-o fa-fw"></i> KS AKTIF<span class="fa arrow"></span></a>
                <ul id="barksaktif" class="nav nav-second-level panel-collapse collapse" >
                    <li id="">
                        <a href="{!! handles('entree::transaction/movementout/filtering/1') !!}">Pergerakan Keluar</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::transaction/movementin/filtering/1') !!}">Pergerakan Masuk</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/move-rfid-gantry/1') !!}">Log Pergerakan RFID / Gantry</a>
                    </li>
                </ul>
            </li>

            @endif
            <!-- end KS AKTIF  -->
            <!-- start KS KUS -->
            @if(Auth::isAny([
                Role::administrator()->name,
                Role::admin()->name,
                Role::pengarahBPRTM()->name,
                Role::timbPengBPRTM()->name,
                Role::ketuaCAROS()->name,
                Role::CarosOfficer()->name
                
            ]))
            <li>
                <a href="#barkskus" data-toggle="collapse"><i class="fa fa-table fa-fw"></i> KS KUS<span class="fa arrow"></span></a>
                <ul id="barkskus" class="nav nav-second-level panel-collapse collapse">
                    <li>
                        <a href="{!! handles('entree::kskus') !!}">Pendaftaran KS KUS</a>
                    </li>

                    <li>
                        <a href="{!! handles('entree::rfid/register/2') !!}">Pendaftaran RFID</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/ks-item/2') !!}">Kemaskini</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::kskus/searchcancel') !!}">Pembatalan KS KUS</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::kskus/application') !!}">Senarai Pinjaman</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::transaction/kskus/application/9') !!}">Pergerakan Keluar</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::transaction/kskus/application/10') !!}">Pergerakan Masuk</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::kskus/borrowing') !!}">Permohonan KS KUS</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/move-rfid-gantry/2') !!}">Log Pergerakan RFID / Gantry</a>
                    </li>
                </ul>
            </li>
            @elseif(Auth::isAny([
                Role::pprNegeri()->name,

                Role::pprHQ()->name,
                Role::pprCawHQ()->name,
                Role::pprHQSiasatan()->name

            ]))

            <li>
                <a href="#barkskus" data-toggle="collapse"><i class="fa fa-table fa-fw"></i> KS KUS<span class="fa arrow"></span></a>
                <ul id="barkskus" class="nav nav-second-level panel-collapse collapse">
                    <li>
                        <a href="{!! handles('entree::transaction/kskus/application/9') !!}">Pergerakan Keluar</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::transaction/kskus/application/10') !!}">Pergerakan Masuk</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::kskus/borrowing') !!}">Permohonan KS KUS</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/move-rfid-gantry/2') !!}">Log Pergerakan RFID / Gantry</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::kskus/application') !!}">Senarai Pinjaman</a>
                    </li>
                </ul>
            </li>

            @else

            @endif
            <!-- end KS KUS -->

            <!-- start eksibit -->
            @if(Auth::isAny([
                Role::administrator()->name,
                Role::admin()->name,
                Role::pengarahBPRTM()->name,
                Role::timbPengBPRTM()->name,
                Role::ketuaCAROS()->name,
                Role::CarosOfficer()->name,
                Role::pprNegeri()->name,
                Role::pprCawNeg()->name,
                Role::pprHQ()->name,
                Role::pprCawHQ()->name,
                Role::pprHQSiasatan()->name
            ]))
            <li>
                <a href="#barexhibit" data-toggle="collapse"><i class="fa fa-keyboard-o fa-fw"></i> EKSIBIT<span class="fa arrow"></span></a>
                <ul id="barexhibit" class="nav nav-second-level panel-collapse collapse">

                    <li>
                        <a href="{!! handles('entree::rfid/register/3') !!}">Pendaftaran RFID & Daftar Seal</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/ks-item/3') !!}">Kemaskini</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::transaction/movementout/filtering/3') !!}">Pergerakan Keluar</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::transaction/movementin/filtering/3') !!}">Pergerakan Masuk</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::kskus/searchreturn') !!}">Pemulangan Eksibit</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/move-rfid-gantry/3') !!}">Log Pergerakan RFID / Gantry</a>
                    </li>

                </ul>
            </li>
            @elseif(Auth::isAny([

            ]))

            <li>
                <a href="#barexhibit" data-toggle="collapse"><i class="fa fa-keyboard-o fa-fw"></i> EKSIBIT<span class="fa arrow"></span></a>
                <ul id="barexhibit" class="nav nav-second-level panel-collapse collapse">


                    <li>
                        <a href="{!! handles('entree::transaction/movementout/filtering/3') !!}">Pergerakan Keluar</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::transaction/movementin/filtering/3') !!}">Pergerakan Masuk</a>
                    </li>
                    <li id="">
                        <a href="{!! handles('entree::rfid/move-rfid-gantry/3') !!}">Log Pergerakan RFID / Gantry</a>
                    </li>


                </ul>
            </li>

            @endif

            <!-- end eksibit -->
            <!-- start laporan -->
            @if(Auth::isAny([
            Role::administrator()->name,
            Role::admin()->name,
            Role::pengarahBPRTM()->name,
            Role::timbPengBPRTM()->name,
            Role::ketuaCAROS()->name,
            Role::CarosOfficer()->name,
            Role::pengarahNegeri()->name,
            Role::TimbPengOperasi()->name,
            Role::ketuaSiasatanNegeri()->name,
            Role::ketuaCawNeg()->name,
            Role::pengBahagian()->name,
            Role::timbPeng()->name,
            Role::ketuaCaw()->name,
            Role::pprNegeri()->name,
            Role::pprCawNeg()->name,
            Role::pprHQ()->name,
            Role::pprCawHQ()->name,
            Role::pprHQSiasatan()->name
            ]))
            <li>
                <a href="#barlaporan" data-toggle="collapse"><i class="fa fa-edit fa-fw"></i> Laporan<span class="fa arrow"></span></a>
                <ul id="barlaporan" class="nav nav-second-level panel-collapse collapse">
                    <li>
                        <a href="{!! handles('entree::report/rfid') !!}">Laporan Daftar RFID</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::report/out_in/1') !!}">Laporan Keluar / Masuk KS Aktif</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::report/borrowing/kskus') !!}">Laporan Pinjaman KS KUS</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::report/out_in/2') !!}">Laporan Keluar / Masuk KS KUS</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::report/out_in/3') !!}">Laporan Keluar / Masuk Eksibit</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::report/reportcount') !!}">Laporan Pendaftaran RFID</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::report/eksibit') !!}">Laporan Eksibit</a>
                    </li>
                </ul>
            </li>
            @endif
            <!-- end laporan -->
            @if(Auth::isAny([
                Role::administrator()->name,
                Role::admin()->name,
                Role::CarosOfficer()->name,
                Role::pengarahBPRTM()->name,
                Role::timbPengBPRTM()->name,
                Role::ketuaCAROS()->name,
            ]))
            <li>
                <a href="#barpengsis" data-toggle="collapse"><i class="fa fa-wrench fa-fw"></i>Pengurusan Sistem<span class="fa arrow"></span></a>
                <ul id="barpengsis" class="nav nav-second-level panel-collapse collapse">
                    <li>
                        <a href="{!! handles('entree::manage/userlist') !!}">Pengurusan Pengguna</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::manage/gantry') !!}">Pengurusan Gantry</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::manage/calendar') !!}">Pengurusan Kalendar</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::manage/settime') !!}">Pengurusan Set Masa</a>
                    </li>
                    <li>
                        <a href="{!! handles('entree::manage/handheldlist') !!}">Pengurusan Handheld</a>
                    </li>
                </ul>
            </li>
            @endif



        </ul>
    </div>
<!-- end menu -->
<!-- start kalendar -->


<div id="side-bar-mid" class="sidebar-nav navbar-collapse">
    <ul class="nav" id="side-menu">
        <li>
            <a href="#profilex" data-toggle="collapse" ><i class="fa fa-user"></i> Kalendar</a>
            <ul id="profile1" class="nav nav-second-level panel-collapse collapse in" >
            <br/><br/>
                <div class="panel panel-primary">

                <div id="home-small" class="lorem-options ">
                  <div class="wrapper1">
                   <div class="responsive-calendar">
                   <div class="controls" style="padding-top:9px">
                       <a class="pull-left" data-go="prev"><div class="btn" style="margin-top:-5px"> << </div></a>
                       <h4>
                       <span data-head-month ></span> <span data-head-year></span>
                       </h4>
                       <a class="pull-right" data-go="next"><div class="btn" style="margin-top:-5px"> >> </div></a>
                   </div><!-- <hr> -->
                   <div class="day-headers">
                     <div class="day header">I</div>
                     <div class="day header">S</div>
                     <div class="day header">R</div>
                     <div class="day header">K</div>
                     <div class="day header">J</div>
                     <div class="day header">S</div>
                     <div class="day header">A</div>
                   </div>
                   <div class="days" data-group="days">

                   </div>
                 </div>
                </div>
                 <h5 class="modal-title" id="headv"></h5>
                </div>
                


                </div>
            </ul>
        </li>
    </ul>
</div>
<!-- end kalendar -->


 


</div>

@endif