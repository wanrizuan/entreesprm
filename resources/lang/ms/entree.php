<?php 

return [
	'login' => [
		'username' => 'ID Pengguna / Emel',
		'emel' => 'Emel',
		'password' => 'Kata Laluan',
		'forgot-password' => 'Lupa Kata Laluan',
		'button' => [
			'signin' => 'Daftar Masuk'
		]
	],
	'password' => [
		'reset' => [
			'title' => 'Tukar Kata Laluan',
			'desc' => 'Masukan Emel Yang Sah Untuk Menukar Kata Laluan',
			'button' => 'Tukar'	
		],
		'form' => [
			'current' => 'Kata Laluan Sedia Ada',
			'new' => 'Kata Laluan Baru',
			'confirm' => 'Pengesahan Kata Laluan'
		]
	],
	'user' => [
		"manage" => "Pengurusan Pengguna",
		"grid" => [
			'username' => 'ID Pengguna',
			'email' => 'Emel Pengguna',
			'role' => 'Peranan',
			'fullname' => 'Nama Pengguna',
		]
	],
	'button' => [
		'save' => 'Simpan',
		'update' => 'Kemaskini',
		'delete' => 'Hapus',
		'reset' => 'Tukar',
	],
	'required' => 'Perlu Diisi'

];