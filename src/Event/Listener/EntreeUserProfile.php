<?php namespace Threef\Entree\Event\Listener;

use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class EntreeUserProfile
{

    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle User Profile Creation Event
     *
     * @param  threef.user.profile  $event
     * @return void
     */
    public function handle($table)
    {
        // Add additional user profile base on project here
        $table->integer('fk_lkp_state')->nullable();
        $table->integer('fk_lkp_branch')->nullable();
        $table->integer('fk_lkp_department')->nullable();
        // $table->string('name',255)->nullable();
        $table->string('digital_sign_filename',255)->nullable();
        $table->string('ic_number',15)->nullable();
        $table->string('office_no',15)->nullable();
        $table->string('hp_number',15)->nullable();



    }



}
