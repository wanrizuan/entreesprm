<?php namespace Threef\Entree\Http\Validation;

use Orchestra\Foundation\Validation\AuthenticateUser as Validation;

class ValidatorUser extends Validation
{
    /**
     * List of rules.
     *
     * @var array
     */
    protected $rules = [];

    /**
     * On login front end scenario.
     *
     * @return void
     */
    protected function onLoginfront()
    {
        $this->rules['password'] = ['required'];
        $this->rules['username'] = ['required'];
    }


    /**
     * On login scenario.
     *
     * @return void
     */
    protected function onLogin()
    {
        $this->rules['password'] = ['required'];
        // $this->rules['username'] = ['required'];
        $this->rules['email'] = ['required','email'];
    }

    /**
     * On login scenario.
     *
     * @return void
     */
    protected function onRegister()
    {
        $this->rules['password'] = ['required','digits:6'];
        $this->rules['username'] = ['required'];
    }

    /**
     * On login scenario.
     *
     * @return void
     */
    protected function onCreate()
    {
        $this->rules['password'] = ['required','digits:6'];
        $this->rules['username'] = ['required'];
        $this->rules['email'] = ['required'];
    }
}