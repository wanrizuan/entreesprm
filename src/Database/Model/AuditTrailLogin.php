<?php namespace Threef\Entree\Database\Model;

use Illuminate\Database\Eloquent\Model;

class AuditTrailLogin extends Model
{
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'audit_trail_login';
}
